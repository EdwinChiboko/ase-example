

---

# AES Example

Welcome to the **AES Example** repository! This project does utilize AES for encription and decription of data.

## Getting Started

Follow these steps to clone, install dependencies, and run the project.

### Prerequisites

- [Java](https://www.oracle.com/java/technologies/javase-downloads.html) installed
- [Maven](https://maven.apache.org/download.cgi) installed (for managing dependencies)

### Clone the Project

```bash
git clone https://github.com/your-username/project-name.git
```

### Install Required Plugins/Dependencies
Hover on the red marked characters and your idea will prompt you to install the plugins.(It will atomaticly handle it)

### Run the Project

Navigate to the project's main class file and run it. You can also change the input string and observe the boolean check and resulting `plainText` output.

```bash
cd path/to/project-name
java -cp target/classes com.example.MainClass
```

Replace `com.example.Main` with the appropriate package and class name of your main class.

## Usage

It can be used for security purpose in data encription and decription from client side to server side

## Contributing



## License

